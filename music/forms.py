from django import forms
from .models import Album, Song, Playlist


class AlbumForm(forms.ModelForm):
    # create metaclass
    class Meta:
        # specify model to be used
        model = Album

        # 'cover' input will be taken using pillow file uploader

        fields = ['name', 'cover', 'artist']

        widgets = {

            'name': forms.TextInput(attrs={
                'class': 'formfield',
                'placeholder': 'Album Name',
            }),

            'artist': forms.TextInput(attrs={
                'class': 'formfield',
                'placeholder': 'Artist',
            }),

        }


class SongForm(forms.ModelForm):
    # create metaclass
    class Meta:
        # specify model to be used
        model = Song

        fields = ['name', 'album', 'featured_artist', 'is_liked']

        widgets = {

            'name': forms.TextInput(attrs={
                'class': 'formfield',
                'placeholder': 'Song Name',
            }),

            'featured_artist': forms.TextInput(attrs={
                'class': 'formfield',
                'placeholder': 'Featured Artist (optional)',
            }),

            'is_liked': forms.CheckboxInput(attrs={
                'class': 'form-check-input',
            }),

            # all songs are associated with an album, and are created through the album's page, hence we do not need the user to fill out the song's album

            'album': forms.HiddenInput()

        }


class PlaylistForm(forms.ModelForm):
    songs = forms.ModelMultipleChoiceField(
        queryset=Song.objects.all(),
        widget=forms.SelectMultiple(attrs={
            'class': 'formfield-multiselect formfield',
        }),
        required=False
    )

    class Meta:
        model = Playlist
        fields = ['name', 'songs']

        # 'songs' input will be handled by a drop-down selector

        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'formfield',
                'placeholder': 'Playlist Name',
            })
        }

async function toggleLiked(song_id){
    $.ajax({
        url: '/music/toggleliked',
        type: 'get',
        data: {
            song_id: song_id,
        },
        success: function(response){
            var songRow = $("#song-row-" + response.sid);

            if(response.liked == true)
            {
                // removes from the table only if it is a table of liked songs
                if ($("#other-songs-table").length) {
                    songRow.detach()
                    // adds it to the other table, if there is another table to add it to
                    if ($("#liked-songs-table").length) {
                        songRow.appendTo("#liked-songs-table");
                    }
                }
                // changes the text on the button
                $("#like-button-" + response.sid).val("Un-like");
                // changes the colour of the text, if the text had colour to begin with
                if (songRow.hasClass("not-liked")){
                    songRow.removeClass("not-liked");
                    songRow.addClass("liked");
                }
            }
            else
            {
                // reverse of above
                if ($("#liked-songs-table").length) {
                    songRow.detach()
                    if ($("#other-songs-table").length) {
                        songRow.appendTo("#other-songs-table");
                    }
                }
                $("#like-button-" + song_id).val("Like");
                if (songRow.hasClass("liked")){
                    songRow.removeClass("liked");
                    songRow.addClass("not-liked");
                }
            }
        }
    });
}

async function deleteSong(song_id){
    $.ajax({
        url: '/music/deletesong',
        type: 'get',
        data: {
            song_id: song_id,
        },
        success: function(response){

            if(response.delete_success == true)
            {
                $("#song-row-" + response.sid).hide();
            }
        }
    });
}

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, UpdateView

from .models import Album, Song, Playlist
from .forms import AlbumForm, SongForm, PlaylistForm


# =================================== ALBUMS ====================================

def index_view(request):
    context = {}

    context["album_list"] = Album.objects.all()

    return render(request, "music/index.html", context)


def detail_view(request, nid):
    context = {}
    album = get_object_or_404(Album, pk=nid)
    songs = Song.objects.filter(album=album)

    context = {
        'album': album,
        'songs': songs,
    }

    return render(request, "music/detail_view.html", context)


def create_view(request):
    context = {}
    if request.method == 'POST':
        form = AlbumForm(request.POST, request.FILES)
        if form.is_valid():
            album = form.save()
            messages.add_message(request, messages.SUCCESS, 'Album Added')
            return redirect('album_detail', album.id)
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data, Album not added')
    else:
        form = AlbumForm()

    context["form"] = form
    return render(request, "music/create_view.html", context)


def update_view(request, nid):
    context = {}
    album = get_object_or_404(Album, pk=nid)
    if request.method == 'POST':
        form = AlbumForm(request.POST, request.FILES, instance=album)
        if form.is_valid():
            form.save()
            return redirect('album_detail', pk=album.pk)
    else:
        form = AlbumForm(instance=album)

    context["form"] = form

    return render(request, 'music/update_view.html', context)


def delete_view(request, nid):
    obj = get_object_or_404(Album, id=nid)

    obj.delete()
    messages.add_message(request, messages.SUCCESS, 'Album Deleted')

    return redirect('album_index')


# =================================== SONGS ====================================

def song_index_view(request):
    context = {}

    context["song_list"] = Song.objects.all()

    return render(request, "music/index_song.html", context)


class CreateSongView(CreateView):
    model = Song
    form_class = SongForm
    template_name = 'music/create_view.html'

    def get_initial(self):
        album = Album.objects.get(id=self.kwargs['nid'])
        return {'album': album}

    def get_success_url(self):
        return reverse_lazy('album_detail', kwargs={'nid': self.kwargs['nid']})


class UpdateSongView(UpdateView):
    model = Song
    form_class = SongForm
    template_name = 'music/update_view.html'

    def get_object(self):
        nid = self.kwargs.get('nid')
        return get_object_or_404(Song, id=nid)

    def form_valid(self, form):
        song = form.save()
        messages.add_message(self.request, messages.SUCCESS, 'Song Updated')

        likes_playlist = get_object_or_404(Playlist, name="Liked Songs")

        if song.is_liked:
            if song not in likes_playlist.songs.all():
                likes_playlist.songs.add(song)
        else:
            if song in likes_playlist.songs.all():
                likes_playlist.songs.remove(song)

        likes_playlist.save()
        return redirect('album_detail', nid=song.album.id)

    def get_success_url(self):
        song = self.get_object()
        return reverse_lazy('album_detail', kwargs={'nid': song.album.id})


def song_delete_view(request, nid):
    obj = get_object_or_404(Song, id=nid)

    album_id = obj.album.id

    obj.delete()
    messages.add_message(request, messages.SUCCESS, 'Song Deleted')

    return redirect('album_detail', album_id)


# =================================== PLAYLIST ====================================


def playlist_index_view(request):
    context = {}

    context["playlist_list"] = Playlist.objects.all()

    return render(request, "music/index_playlist.html", context)


def playlist_detail_view(request, nid):
    context = {}
    playlist = get_object_or_404(Playlist, id=nid)

    context = {
        'playlist': playlist,
    }

    return render(request, "music/detail_playlist.html", context)


class CreatePlaylistView(CreateView):
    model = Playlist
    form_class = PlaylistForm
    template_name = 'music/create_view.html'

    def form_valid(self, form):
        # Save the form and get the instance
        response = super().form_valid(form)
        playlist = form.instance
        # Redirect to the playlist detail view
        return redirect('playlist_detail', nid=playlist.pk)

    def get_success_url(self):
        # This is not used in this case, but required by CreateView
        return reverse_lazy('playlist_index')


def playlist_delete_view(request, nid):
    obj = get_object_or_404(Playlist, id=nid)

    obj.delete()
    messages.add_message(request, messages.SUCCESS, 'Playlist Deleted')

    return redirect('playlist_index')


class UpdatePlaylistView(UpdateView):
    model = Playlist
    form_class = PlaylistForm
    template_name = 'music/update_view.html'

    def get_object(self):
        nid = self.kwargs.get('nid')
        return get_object_or_404(Playlist, id=nid)

    def get_success_url(self):
        nid = self.object.id
        return reverse_lazy('playlist_detail', kwargs={'nid': nid})

# =================================== AJAX ====================================


class ToggleLikedView(View):
    def get(self, request):
        sid = request.GET.get('song_id')
        song = get_object_or_404(Song, pk=sid)

        song.is_liked = not song.is_liked
        song.save()

        likes_playlist = get_object_or_404(Playlist, name="Liked Songs")

        if song.is_liked:
            if song not in likes_playlist.songs.all():
                likes_playlist.songs.add(song)
        else:
            if song in likes_playlist.songs.all():
                likes_playlist.songs.remove(song)

        return JsonResponse({'liked': song.is_liked, 'sid': sid}, status=200)


class DeleteSongView(View):
    def get(self, request):
        sid = request.GET.get('song_id')
        try:
            song = get_object_or_404(Song, pk=sid)
        except Song.DoesNotExist:
            return JsonResponse({'delete_success': False, 'sid': sid}, status=200)

        song.delete()
        return JsonResponse({'delete_success': True, 'sid': sid}, status=200)

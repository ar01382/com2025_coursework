from django.core.management import call_command
from django.test import TestCase
from django.db.backends.sqlite3.base import IntegrityError
from django.db import transaction
from .models import *
from django.urls import reverse
from .forms import *


class AlbumTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        call_command('seed')

# ============ TEST SEEDING ==============

    def test_album_count(self):
        # Check if albums were seeded correctly
        album_count = Album.objects.count()
        self.assertEquals(album_count, 3, "Different number of album(s) than expected")

    def test_song_count(self):
        # Check if songs were seeded correctly
        song_count = Song.objects.count()
        self.assertEquals(song_count, 41, "Different number of song(s) than expected")

    def test_playlist_count(self):
        # Check if playlists were seeded correctly
        playlist_count = Playlist.objects.count()
        self.assertEquals(playlist_count, 1, "Different number of playlist(s) than expected")

    # ============ TEST ALBUM ==============

    def test_save_album(self):
        db_count = Album.objects.all().count()
        album = Album(name="Test Album", artist="Tester")
        album.save()
        self.assertEqual(db_count + 1, Album.objects.count())

    def test_create_album(self):
        context = {
            'name': 'Test Album',
            'artist': 'Tester',
        }
        form = AlbumForm(context)
        self.assertTrue(form.is_valid())

    def test_duplicate_album(self):
        context = {
            'name': 'Discovery',
            'artist': 'Daft Punk',
        }
        form = AlbumForm(context)
        self.assertFalse(form.is_valid())

    def test_empty_album(self):
        context = {}
        form = AlbumForm(context)
        self.assertFalse(form.is_valid())

# ============ TEST SONGS ==============

    def test_create_song(self):
        context = {
            'name': 'Test Song',
            'album': Album.objects.get(name='Discovery'),
            'featured_artist': 'Tester',
            'is_liked': True
        }
        form = SongForm(context)
        self.assertTrue(form.is_valid())

    def test_create_song_defaults(self):
        context = {
            'name': 'Test Song',
            'album': Album.objects.get(name='Discovery')
        }
        form = SongForm(context)
        self.assertTrue(form.is_valid())

    def test_create_empty_song(self):
        context = {
            'album': Album.objects.get(name='Discovery')
        }
        form = SongForm(context)
        self.assertFalse(form.is_valid())

# ============ TEST PLAYLISTS ==============

    def test_create_playlist(self):
        context = {
            'name': 'Test Playlist',
            'songs': Song.objects.all(),
        }
        form = PlaylistForm(context)
        self.assertTrue(form.is_valid())

    def test_likes_playlist(self):
        self.assertEquals(Song.objects.filter(is_liked=True).count(),
                          Playlist.objects.get(name='Liked Songs').songs.count())

    def test_empty_playlist(self):
        context = {
        }
        form = PlaylistForm(context)
        self.assertFalse(form.is_valid())

from django.db import models


class Album(models.Model):
    name = models.CharField(max_length=128)
    cover = models.ImageField(upload_to='album_covers', default="cover-backup.png")
    artist = models.CharField(max_length=128)

    class Meta:
        unique_together = ('name', 'artist')


class Song(models.Model):
    name = models.CharField(max_length=128)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    featured_artist = models.CharField(max_length=128, blank=True, null=True)
    is_liked = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'album')


class Playlist(models.Model):
    name = models.CharField(max_length=128, unique=True)
    songs = models.ManyToManyField(Song)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [models.Index(fields=['name'])]

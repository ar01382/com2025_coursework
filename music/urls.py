from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

urlpatterns = [
    # music/album
    path('album', views.index_view, name='album_index'),
    # music/album/id
    path('album/<int:nid>', views.detail_view, name='album_detail'),
    # music/album/new
    path('album/new', views.create_view, name='album_new'),
    # music/album/edit/id
    path('album/edit/<int:nid>', views.update_view, name='album_update'),
    # music/album/delete/id
    path('album/delete/<int:nid>', views.delete_view, name='album_delete'),
    # music/song
    path('song', views.song_index_view, name='song_index'),
    # music/album/id/song/add
    path('album/<int:nid>/song/add', views.CreateSongView.as_view(), name='song_new'),
    # music/song/id
    path('song/<int:nid>/update', views.UpdateSongView.as_view(), name='song_update'),
    # music/song/id/delete
    path('song/<int:nid>/delete', views.song_delete_view, name='song_delete'),
    # music/playlist
    path('playlist', views.playlist_index_view, name='playlist_index'),
    # music/playlist/create
    path('playlist/create', views.CreatePlaylistView.as_view(), name='playlist_new'),
    # music/playlist/edit/id
    path('playlist/edit/<int:nid>', views.UpdatePlaylistView.as_view(), name='playlist_update'),
    # music/playlist/delete/id
    path('playlist/delete/<int:nid>', views.playlist_delete_view, name='playlist_delete'),
    # music/playlist/id
    path('playlist/<int:nid>', views.playlist_detail_view, name='playlist_detail'),
    # music/toggleliked
    path('toggleliked', views.ToggleLikedView.as_view(), name='toggle_liked'),
    # music/deletesong
    path('deletesong', views.DeleteSongView.as_view(), name='delete_song'),

]

import glob
import os
from pathlib import Path
import json

from django.conf import settings
from django.core.files.images import ImageFile
from django.core.management.base import BaseCommand
from django.shortcuts import get_object_or_404

from music.models import *

ROOT_DIR = Path('music') / 'management' / 'commands'


class Command(BaseCommand):

    def handle(self, *args, **options):
        # clears the database, songs don't need to be removed as they cascade with their associated album
        Album.objects.all().delete()
        Playlist.objects.all().delete()
        # checking that all songs were removed
        assert not Song.objects.all()

        media_path = os.path.join(settings.MEDIA_ROOT, 'album_covers')

        if not os.path.exists(media_path):
            self.stdout.write(self.style.ERROR(f'Path does not exist: {media_path}'))
            return

        files = glob.glob(os.path.join(media_path, '*'))

        for file_path in files:
            try:
                os.remove(file_path)
            except Exception as e:
                self.stdout.write(self.style.ERROR(f'Error deleting {file_path}: {e}'))

        self.stdout.write(self.style.SUCCESS('All files in media/album_covers/ have been cleared'))

        # loads the sample data
        with open(ROOT_DIR / 'sample_data.json') as json_file:
            sample_data = json.load(json_file)
        # loops through the sample data adding and saving the kwargs
        for album in sample_data['albums']:
            kwargs = {'name': album['name'],
                      'cover': album['cover'],
                      'artist': album['artist']}
            # sets the cover kwarg to the actual image file referenced in sample_data.json
            image_path = album['cover']
            if image_path is not None:
                kwargs['cover'] = ImageFile(open(ROOT_DIR / image_path, 'rb'),
                                            name=image_path)
            # saves the album with the initialised variables
            Album(**kwargs).save()

        for song in sample_data['songs']:
            kwargs = {'name': song['name'],
                      'album': get_object_or_404(Album, name=song['album']),
                      'featured_artist': song['featured_artist'],
                      'is_liked': song['is_liked']}

            Song(**kwargs).save()

        # initialised the liked songs playlist with every liked song in the sample dataset
        p = Playlist(name="Liked Songs", id=1)
        # saves the playlist so that the loop function can .add songs to it
        p.save()

        for song in Song.objects.all():
            if song.is_liked:
                p.songs.add(song)

        p.save()

        # prints to console for debugging
        self.stdout.write('done.')

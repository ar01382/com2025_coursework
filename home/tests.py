from django.core import mail
from django.test import TestCase
from django.urls import reverse


class HomePageTests(TestCase):

    def setUP(self):
        return

    def test_homepage(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

        self.assertContains(response, 'Spotty.fi')
        self.assertContains(response, 'Welcome to my music organiser!')
        self.assertContains(response, 'Thanks for visiting my site')

    def test_contact(self):
        response = self.client.get(reverse('contact'))
        self.assertEquals(response.status_code, 200)

        self.assertContains(response, 'Spotty.fi')
        self.assertContains(response, 'Get in touch')
        self.assertContains(response, 'Thanks for visiting my site')


class EmailTest(TestCase):
    def test_email(self):
        mail.send_mail("Test Subject", "Test Body", "test@test.test", ['test2@test2.test'])
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "Test Subject")
